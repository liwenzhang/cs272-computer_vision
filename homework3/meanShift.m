function abs = meanShift()

rng(2);
FEATURELEN = 16560;
IMGNUM = 2452;
THRESHOULD = 3;
C = 38; % categories
cd('CroppedYale');

% load file
imgMap = containers.Map();
allnames = struct2cell(dir);   % dos命令dir列出所有的文件，用struct2cell转换为元胞数组
[~, n] = size(allnames);
var1 = [];
imgMatrix = zeros(IMGNUM,FEATURELEN);
var2 = 0;
for i=3:n
    name = allnames{1, i};
    cd(name);
    imgNames = struct2cell(dir);
    cnt=0;
    [~, nn] = size(imgNames);
    for j=3:nn
        imgName = imgNames{1, j};
        if ~isempty(strfind(imgName,'pgm'))
            if ~isempty(strfind(imgName,'bad'))
                continue
            end
            cnt = cnt+1;
            var2 = var2 + 1;
            img = imread(imgName, 'pgm');
            % HOG features
            img = imresize(img, [192, 168], 'bicubic');
            featureVector = extractHOGFeatures(img);
            imgMatrix(var2, :) = featureVector;
            imgMap(imgName) = featureVector;
        end
    end
    var1 = [var1, cnt];
    cd('..');
end
cd('..');


hog_coeff = pca(imgMatrix);
img = imgMatrix * hog_coeff(:, 1:100);

label = label_truth(var1);

R = 52.75;
[N, ~] = size(img);
idx = zeros(N, 1);
idxCnt = 0;
for i = 1:N
    if idx(i) == 0
        center = img(i, :);
        idxMove = 1;
        while sum(idxMove) ~= 0
            dis = sum((img - repmat(center, N, 1)).^2, 2);
            idxCal = dis < R;
            idxMove = idxCal & idx == 0;
            idx(idxMove) = -1;
            center = sum(img(idxCal, :), 1) / sum(idxCal);
        end
        if sum(idxCal & idx == -1) * 2 > sum(idxCal) 
            idxCnt = idxCnt + 1;
            new_id = idxCnt;
        else
            hist = histcounts(idx(idxCal & idx ~= -1));
            new_ids = find(hist == max(hist));
            new_id = new_ids(1);
        end
        idx(idx == -1) = new_id;
    end
end

disp(idxCnt);
purity(label, idx)
[TP, FP, TN, FN] = confusion_matrix(label, idx);
end

function [ TP, FP, TN, FN ] = confusion_matrix( label, idx )
TP = 0;
FP = 0;
TN = 0;
FN = 0;
N = length(label);
for i = 1:N-1
    truth = label == label(i);
    pred = idx == idx(i);
    
    truth = truth(i+1:N);
    pred = pred(i+1:N);
    TP = TP + sum(truth & pred);
    FP = FP + sum(~truth & pred);
    TN = TN + sum(~truth & ~pred);
    FN = FN + sum(truth & ~pred);
end
end

function [ pu ] = purity( label, idx )
pu = 0;
for i=1:length(idx)
    if sum(idx == i) == 0
        break
    end
    cnt = 0;
    for j=1:length(label)
        if sum(label == j) == 0
            break
        end
        cnt = max(cnt, sum((idx == i) & (label == j)));
    end
    pu = pu + cnt;
end
pu = pu / length(label);
histcounts(idx)
end

function label = label_truth(var1)
label = [];
    for i=1:length(var1)
        tmp_label = [];
        for j=1:var1(i)
            tmp_label(j)=i;
        end
        label = [label, tmp_label];
    end
    label = label';
end
