function f1 =  kmeansZLW()
rng(2);
FEATURELEN = 3564;
IMGNUM = 2452;
THRESHOULD = 3;
C = 38; % categories
cd('CroppedYale');
% load file
imgMap = containers.Map();
allnames = struct2cell(dir);   % dos命令dir列出所有的文件，用struct2cell转换为元胞数组
[~, n] = size(allnames);
var1 = [];
imgMatrix = zeros(IMGNUM,FEATURELEN);
var2 = 0;
for i=3:n
    name = allnames{1, i};
    cd(name);
    imgNames = struct2cell(dir);
    cnt=0;
    [~, nn] = size(imgNames);
    for j=3:nn
        imgName = imgNames{1, j};
        if ~isempty(strfind(imgName,'pgm'))
            if ~isempty(strfind(imgName,'bad'))
                continue
            end
            cnt = cnt+1;
            var2 = var2 + 1;
            img = imread(imgName, 'pgm');
            % HOG features
            img = imresize(img, [192, 168], 'bicubic');
            featureVector = extractHOGFeatures(img, 'CellSize',[16,16]);
            imgMatrix(var2, :) = featureVector;
            imgMap(imgName) = featureVector;
        end
    end
    var1 = [var1, cnt];
    cd('..');
end
cd('..');
% get init centers
label = label_truth(var1);
%test
[idx,~] = kmeans(imgMatrix,38);
% evaluation(idx', var1)
[ TP, FP, TN, FN ] = confusion_matrix( label, idx')
acc =  TP/(TP+FP)
recall = TP/(TP + TN)
purity(label, idx')
centerTable = zeros(1, C);
centerTable(1) = int16(rand() * IMGNUM);
distMap = pdist2(imgMatrix , imgMatrix);
for i = 2:C
    distSum = zeros(1, IMGNUM);
    for j=1:i-1
        distSum = distSum + distMap(centerTable(j), :);
    end
    allsum = sum(distSum);
    tmpr = rand() * allsum;
    for k=1:IMGNUM
        if tmpr < 0
            break
        end
        tmpr = tmpr - distSum(k);
    end
    centerTable(i) = k;
end
%kmeans
centers = zeros(C, FEATURELEN);
for i = 1:C
    centers(i, :) = imgMatrix(centerTable(i),:);
end
for thre=1:10
tmpDist = pdist2(centers, imgMatrix);
[~, Index] = min(tmpDist);
diff = 0.0;
for i=1:C
    [~, indexr] = find(Index==i);
    len = length(indexr);
    tmpc = zeros(1, FEATURELEN);
    for j=1:len
        tmpc = tmpc + imgMatrix(indexr(j), :);
    end
    tmpc = tmpc./len;
    diff =diff + norm(tmpc - centers(i, :));
    centers(i, :) = tmpc;
end
[ TP, FP, TN, FN ] = confusion_matrix( label, Index)
acc =  TP/(TP+FP)
recall = TP/(TP + TN)
purity(label, Index)
end
end

function r = evaluation(calMat, claTable)
    rightNum = 0;
    rightType = zeros(1, 38);
    for i=1:38
        [~, index] = find(calMat==i);
        calNum = zeros(1, 38);
        num =0;
        for j=1:38
            [~, index2] = find(index > num & index < (num + claTable(j)));
            
            calNum(j) = length(index2);
            num = num + claTable(j);
        end
        [typeVal, type] = max(calNum);
        rightNum = rightNum + typeVal;
        rightType(i) = type;
    end
    sum(claTable)
    r = rightNum / sum(claTable);
    rightType
end

function [ TP, FP, TN, FN ] = confusion_matrix( label, idx )
TP = 0;
FP = 0;
TN = 0;
FN = 0;
N = length(label);
for i = 1:N-1
    truth = label == label(i);
    pred = idx == idx(i);
    
    truth = truth(i+1:N);
    pred = pred(i+1:N);
    TP = TP + sum(truth & pred);
    FP = FP + sum(~truth & pred);
    TN = TN + sum(~truth & ~pred);
    FN = FN + sum(truth & ~pred);
end
end

function label = label_truth(var1)
label = [];
    for i=1:length(var1)
        tmp_label = [];
        for j=1:var1(i)
            tmp_label(j)=i;
        end
        label = [label, tmp_label];
    end
end

function [ pu ] = purity( label, idx )
pu = 0;
for i=1:length(idx)
    if sum(idx == i) == 0
        break
    end
    cnt = 0;
    for j=1:length(label)
        if sum(label == j) == 0
            break
        end
        size((idx==i))
        cnt = max(cnt, sum((idx == i) & (label == j)));
    end
    pu = pu + cnt;
end
pu = pu / length(label);
histcounts(idx)
end