function [] = part1(filename)
%author: Ehaschia
% 2017/03/06
% Implement Saliency Detection: A Spectral Residual Approach
img = imread(filename);


inImg = im2double(rgb2gray(img));
[rows, cols]=size(inImg);
inImg = imresize(inImg, [64, 64], 'bilinear');
img_ftt = fft2(inImg); 
img_amp = log(abs(img_ftt));
img_phase = angle(img_ftt);
img_target= img_amp - imfilter(img_amp, fspecial('average', 3), 'replicate'); 
img_saliency = abs(ifft2(exp(img_target + 1i * img_phase))).^2;
% img_saliency = imfilter(img_saliency, fspecial('gaussian', 3));
img_saliency = mat2gray(img_saliency);
img_saliency = imresize(img_saliency, [rows cols], 'bilinear');

salMap=im2double(img_saliency);
figure  
imshow(salMap),
end