% author: Ehaschia
% date: 2017/03/02
% Implement the papaer "Context-Aware Saliency Detection" by patch
function S = part2(imgName)
% read img and convert to LAB color space
I = imread(imgName);
I = imresize(I, [size(I, 1) size(I, 2)]/max(size(I, 1), size(I, 2))*120, 'bilinear');
I = rgb2lab(I);

I = im2double(I);



THRESHOULD = 0.2;
K = 64;
IMSCALE = [1 0.8 0.5 0.3];
PASCALE = [1, 0.5, 0.25];
S = zeros(size(I,1), size(I,2), length(IMSCALE));
tmpS = zeros(length(PASCALE), size(I, 1), size(I, 2), K);

for i=1:length(IMSCALE)
    for j= 1: length(PASCALE)
        j
        if IMSCALE(i) * PASCALE(j) > THRESHOULD
            if  IMSCALE(i)<1
                stmp = Saliency_internal(imresize(I, IMSCALE(i), 'bilinear'), IMSCALE(i), PASCALE(j));
                tmpS(j, :, :, :) = imresize(stmp, [size(I,1), size(I,2)], 'bilinear');
            else
                tmpS(j, :, :, :) = Saliency_internal(I, IMSCALE(i), PASCALE(i));
            end
        end      
    end
    for ii=1:size(tmpS, 2)
        for jj=1:size(tmpS, 3)
            aVal = [];
            for kk = 1:size(tmpS, 1)
                aVal = [aVal; tmpS(kk, ii, jj, :)];
            end
            aVal = sort(aVal, 'ascend');
            S(ii, jj, i) = 1 - exp(-mean(aVal(1:K)));
        end
    end
    
end
S = mean(S, 3);

binS = S > max(max(S))*0.8;

% Equation (5) in [1]
D  = bwdist(binS);
D  = D / max(max(D));
S  = S .* (1-D);
S = mat2gray(S);
figure
imshow(S)
                        
                        


function S = Saliency_internal(I, scale, patchScale)

pH  = 3;      % half of patch size, patch size is 7x7
c = 3;      % position constant
K  = 64;     % Select most K similar patches

S = zeros(size(I,1), size(I,2), K);

% Generate position map
PM = zeros(size(I,1), size(I,2), 2);
len = max(size(I,1), size(I,2));
PM(:,:,1) = repmat(1:size(I,2),    [size(I,1) 1]) / len;
PM(:,:,2) = repmat((1:size(I,1))', [1 size(I,2)]) / len;
PM = imresize(PM, patchScale, 'bicubic');

I = imresize(I, patchScale, 'bicubic');
[h, w] = size(I);
for r=pH+1:h-pH
    for c=pH+1:w-pH
            
        % color distance
        Pi    = I(r-pH:r+pH, c-pH:c+pH, :);
        Dc= patchDisCal(Pi, I);
        
        D = Dc./(1 + c.*(sqrt( (c/len-PM(:,:,1)).^2 + (r/len-PM(:,:,2)).^2 )));
        D = D(pH+1:pH:end-pH, pH+1:pH:end-pH);
       
        val = sort(D, 'ascend');
        val = val(1:K);
        S(r,c,:) = val';
    end
end



function D = patchDisCal(P, I)
D = zeros(size(I, 1), size(I, 2));
hM = (size(P, 1)-1)/2;
hN = (size(P, 2)-1)/2;
vP = reshape(P, numel(P), 1);

for r=hM+1:size(I, 1)-hM
    for c=hN+1:size(I, 2)-hN
        tmp = I(r-hM:r+hM, c-hN:c+hN, :);
        vtmp = reshape(tmp, numel(P), 1);
        D(r, c) = norm(vP-vtmp);
    end
end