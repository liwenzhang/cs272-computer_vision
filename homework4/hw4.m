
% [train, test, trainlabel, testlabel] = loadData();
NUMC = 512;
[trainIdx, C] = kmeans(train, NUMC);

%bag of word
trainSize = size(trainIdx, 1)/21;

trainBOW = zeros(trainSize, NUMC);
for i=1:size(trainIdx)
    index = floor((i-1)/21) + 1;
    trainBOW(index, trainIdx(i))=trainBOW(index, trainIdx(i)) + 1;
end 
md1 =  fitcknn(trainBOW, trainlabel, 'NumNeighbors', 5 ,'Standardize',1);

testSize = size(test, 1)/21;
testBOW = zeros(testSize, NUMC);

for i=1:size(test,1)
    index = floor((i-1)/21) + 1;
    dist = pdist2(test(i, :), C);
    [~ , idxC] = min(dist);
    testBOW(index, idxC) = testBOW(index, idxC) + 1;
end

[predictLabel, ~, ~] = predict(md1, testBOW);
tmp = (predictLabel == testlabel);
acc = sum(tmp)/testSize;
