function [res] = colorHis(filename)
file = ['spatial_envelope_256x256_static_8outdoorcategories/',filename];
img = imread(file);
res = zeros(21, 256);
for i=1:21
    image = imgSege(img, i);
    %Split into RGB Channels
    Red = image(:,:,1);
    Green = image(:,:,2);
    Blue = image(:,:,3);
    
    % transfer to image format
    Red = uint8(Red);
    Green = uint8(Green);
    Blue = uint8(Blue);
    
    %Get histValues for each channel
    [yRed, ~] = imhist(Red);
    [yGreen, ~] = imhist(Green);
    [yBlue, ~] = imhist(Blue);

    his = yRed + yGreen + yBlue;
    his = his/norm(his);
    res(i, :) = his;
end
disp(['load ', file, ' suucueeful!']);
end

function image = imgSege(img, index)
if index == 1
    image = img;
end
if index > 1 && index < 6
   [imgR, imgC, ~] = size(img);
   R = floor(imgR / 2);
   C = floor(imgC / 2);
   image = zeros(R, C, 3);
   index = index - 2;
   row = floor(index / 2) + 1;
   col = mod(index, 2) + 1;
   image(:, :, 1) = img(R*(row -1)+1:R*row, C*(col-1)+1:C*col, 1);
   image(:, :, 2) = img(R*(row -1)+1:R*row, C*(col-1)+1:C*col, 2);
   image(:, :, 3) = img(R*(row -1)+1:R*row, C*(col-1)+1:C*col, 3);
end
if index >= 6
   [imgR, imgC, ~] = size(img);
   R = floor(imgR / 4);
   C = floor(imgC / 4);
   image = zeros(R, C, 3);
   index = index - 6;
   row = floor(index / 4) + 1;
   col = mod(index, 4) + 1;
   image(:, :, 1) = img(R*(row -1)+1:R*row, C*(col-1)+1:C*col, 1);
   image(:, :, 2) = img(R*(row -1)+1:R*row, C*(col-1)+1:C*col, 2);
   image(:, :, 3) = img(R*(row -1)+1:R*row, C*(col-1)+1:C*col, 3);
end
end
    