function [train, test, trainlabel, testlabel] = loadData()
% K is the hyperparameter of KNN

f1 = fopen('training_label.txt', 'rt');
trainInfo = textscan(f1, '%s %d');
fclose(f1);
f2 = fopen('testing_label.txt', 'rt');
testInfo = textscan(f2, '%s %d');
fclose(f2);

trainLabel = trainInfo{2};
testLabel = testInfo{2};
trainLen = size(trainLabel,1);
testLen = size(testLabel,1);

train = zeros(trainLen*21, 256);
for i=1:trainLen
    train((i-1)*21 + 1: i*21 , :, :) = colorHis(trainInfo{1}{i});
end
test = zeros(testLen*21, 256);
for i=1:testLen
    test((i-1)*21 + 1: i*21 , :, :) = colorHis(testInfo{1}{i});
end
trainlabel = trainInfo{2};
testlabel = testInfo{2};
end